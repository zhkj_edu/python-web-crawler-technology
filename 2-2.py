import urllib.request
import json

# 金山翻译，http://www.iciba.com/fy
url = 'http://ifanyi.iciba.com/index.php?c=trans&m=fy&client=6&auth_user=key_ciba&sign=8752b798d6ddebba'
dataValue = {
    'from': 'en',
    'to': 'zh',
    'q': 'apple',
}

dataValue = urllib.parse.urlencode(dataValue).encode('utf-8')
response = urllib.request.urlopen(url, data=dataValue)
resp = response.read().decode('utf-8')
print('响应类型：', type(response))
print('响应状态码：', response.getcode())
print('响应内容：', resp)

# string翻译成json
jsonObj = json.loads(resp)

print(jsonObj)