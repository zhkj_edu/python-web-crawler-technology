import chardet
import requests
# https://www.biqukan.la/
r = requests.get('https://www.biqukan.la/')
print(r.encoding)
print(chardet.detect(r.content))

code_type = chardet.detect(r.content)['encoding']
print(r.content.decode(code_type))
