import csv

data = [
    ['姓名', '性别', '生日'],
    ['小明', '男', '2016-06-06'],
]

with open('data.csv', 'w', newline='') as file: #newline如果不设置，则每写入一行会写入一个空行
    writer = csv.writer(file)
    writer.writerows(data)
    writer.writerow(['小红', '女', '2016-06-06'])
