from bs4 import BeautifulSoup
import requests
#正则表达式模块
import re

# 文档:https://www.crummy.com/software/BeautifulSoup/bs4/doc/index.zh.html

url = 'https://beijing.qfang.com/newhouse/list'

r = requests.get(url)
soup = BeautifulSoup(r.text, 'lxml')

print('所有span节点数: ', len(soup.find_all('span')))
print('所有属性为amount的 span节点数: ', len(soup.find_all('span', attrs={'class': 'amount'})))

print('class属性为amount的所有Span节点：')
i = 0
row = 0
for node in soup.find_all('span', attrs={'class': 'amount'}):
    i += 1

    if row % 2 == 0:
        print('-', end='')

    if i % 3 == 0:
        print(node.text)
        row += 1
    else:
        print(node.text, end=' ')

print('---------------------------------------------------')

print('string包含3室的文本 ', soup.find_all(string=re.compile('3室')))