import requests
url = 'https://pythontab.com/'

proxies = [
    {'http': 'http://202.55.5.209:8090'},
    {'http': 'http://106.54.128.253:999'},
    {'http': 'http://47.113.90.161:83'},
]

for proxy in proxies:
    try:
        r = requests.get(url, proxies=proxy)
    except:
        print("此代理地址不可用")
    else:
        print("此代理IP可用：", proxy)
        print("响应状态码：", r.status_code)