import requests
import pretty_errors

try:
    r = requests.get('https://www.douban.com/', timeout=0.01)
except requests.Timeout:
    print('Time OUt!')
except requests.exceptions.ConnectionError as e:
    print('连接错误')
    print(e)
else:
    print('响应状态码：', r.status_code)

