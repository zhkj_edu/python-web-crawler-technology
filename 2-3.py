import urllib.request

url = 'http://fanyi.youdao.com'
headervalues = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36 Edg/99.0.1150.36',
}

request = urllib.request.Request(url, headers=headervalues)
response = urllib.request.urlopen(request)
resp = response.read().decode('utf-8')
print('响应类型：', type(response))
print('响应状态码：', response.getcode())
print('响应内容：', resp)
