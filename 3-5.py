from bs4 import BeautifulSoup
import requests

# 文档:https://www.crummy.com/software/BeautifulSoup/bs4/doc/index.zh.html

url = 'https://beijing.qfang.com/newhouse/list'

r = requests.get(url)
soup = BeautifulSoup(r.text, 'lxml')

title = soup.find('div', class_='list-main-header clearfix').a.text.strip()
print('第一个房源标题: ', title)
