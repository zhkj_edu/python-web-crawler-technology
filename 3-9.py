from bs4 import BeautifulSoup, Tag
import requests


# 获取列表中房源详情
def get_house(house: Tag):
    house_title = house.select_one('[class="list-main-header clearfix"] a').text.strip()
    print(house_title)

    house_metas = house.select('[class="house-metas clearfix"] p')

    for item in house_metas:
        print(item.string.strip())

    list_price = house.select('[class="list-price"] span')
    print(list_price[0].string, list_price[1].string)

    location = house.select_one('[class="house-location clearfix"] div').text.strip().replace('      ', '') \
        .replace('\r\n', '').replace('  ', '')
    print(location)
    print('---------------------------------------')


# ------------------------------------------------------------------
url = 'https://zhuhai.qfang.com/rent'

r = requests.get(url)
soup = BeautifulSoup(r.text, 'lxml')

houses = soup.select('.list-result li')

for house in houses:
    get_house(house)
