import urllib.parse

url = 'https://f.youdao.com/?path=file&keyform=Nav-doc'

parseResult = urllib.parse.urlparse(url)
print(parseResult)
print(urllib.parse.urljoin('https://f.youdao.com/','?path=file&keyfrom=Nav-doc'))

#定义字符串
params = {'path':'file','keyfrom':'Nav-doc'}
print(urllib.parse.urlencode(params))

#分解转换
query = parseResult.query
print(urllib.parse.parse_qs(query))
print(urllib.parse.parse_qsl(query))


# 常见问题，如果参数里有一个变量是url地址怎么办？urllib.parse.quote(url)
url = 'https://f.youdao.com/?wd=网络爬虫'
quoteUrl = urllib.parse.quote(url)
print(quoteUrl)
print(urllib.parse.unquote(quoteUrl))