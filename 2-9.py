import requests

paramValue = {
    'q': 'python',
}

r = requests.get('https://www.douban.com/search', params=paramValue)
print('请求的url：', r.url)
print('响应状态码：', r.status_code)
print('请求头：', r.request.headers)
print(r.content)