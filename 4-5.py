import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

browser = webdriver.Edge()

browser.get('https://www.taobao.com/')

close_btn = browser.find_element(By.LINK_TEXT, '关闭')
if EC.element_to_be_clickable(close_btn):  # 如果按钮可见
    close_btn.click()
# 定位q节点->旧方法
# print(browser.find_element_by_id('q'))
jhs_btn = browser.find_elements(By.CSS_SELECTOR, '.nav-hd li')[1]
print(jhs_btn)

jhs_btn.click()

time.sleep(1)
browser.maximize_window()
time.sleep(3)
