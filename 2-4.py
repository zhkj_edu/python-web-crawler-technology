import urllib.request
import urllib.error

try:
    response = urllib.request.urlopen('http://fanyi.youdao.com/api.htm')
except urllib.error.HTTPError as e:
    print('异常原因：', e.reason)
    print('状态码：', e.code)
    print('请求头：', e.headers)
except urllib.error.URLError as e:
    print(e.reason)
else:
    print('访问成功')
