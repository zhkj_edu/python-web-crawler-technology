import requests
import time
import random

url = 'https://pythontab.com/'

for i in range(1, 10):
    t1 = time.time()
    r = requests.get(url)
    sleep_time = random.randint(0, 2) + random.random()
    time.sleep(sleep_time)
    t2 = time.time()
    print("两次请求时间间隔：", t2 - t1)
    print("响应状态码：", r.status_code)
