import requests
from lxml import etree

# 三国演义
url = 'https://www.bqkan8.com/50_50096/18520412.html'
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36 Edg/98.0.1108.62',
}

r = requests.get(url, timeout=10, headers=headers)
html = etree.HTML(r.text)

print('class属性位content的div节点', html.xpath('//div[@class="content"]'))
print('id属性，以con开头的Div节点', html.xpath('//div[starts-with(@id,"con")]'))
print('id包含tent的节点', html.xpath('//div[contains(@id,"tent")]'))
print('id属性值为content和class为showtxt的div节点', html.xpath('//div[@id="content" and @class="showtxt"]'))
print('id属性值为content节点的class属性值', html.xpath('//div[@id="content"]/@class'))
