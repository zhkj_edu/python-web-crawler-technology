import chardet

str2byte = '网络爬虫'.encode('utf-8')

print(str2byte)
print(type(str2byte))
print(chardet.detect(str2byte))

byte2str = str2byte.decode('utf-8')
print(byte2str)
print(type(byte2str))