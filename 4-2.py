import requests
from bs4 import BeautifulSoup

# 经常请求不到，这里做一个缓存处理-原因是京东反爬虫策略
import requests_cache
requests_cache.install_cache('jd_cache')


url = 'https://search.jd.com/s_new.php?keyword=python&pvid=039f04c1767d43088938245362155473&page=2&s=28&scrolling=y'
headervalue = {
    'Referer': 'https://search.jd.com/search?keyword=python',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.75 Safari/537.36 Edg/100.0.1185.36',
    'Cookie': '__jdu=16448122780871481326052; shshshfpa=17eb52a3-dbc6-3161-aeb0-d6c3e8ce1c98-1644812279; shshshfpb=cfqeTCChXnGl_WVK-xKPZJw; qrsc=3; unpl=JF8EAKlnNSttWksHBxxWGxJCTghSWw0JGB9RZ2JWBF5bQ1cMSQASR0R7XlVdXhRKEh9uYhRXXlNOVQ4bAysSEXtdVV9fCUwfA29uNWRtW0tkBCsCHBcVQl1XXVsBSh4Fb2IMVF9bTFUBGDIrEhhPbWRuWAhKFgRoZANcW2hKZAcaAx4RE0xcVlptSSURBGxiAVQQWExRABICGBEWQlxdWF0NQhcBbGAEUF5oSmQG; __jdv=122270672|www.maigoo.com|t_1003069118_|tuiguang|31b36e11c4d74a0b9c94ba22928c38de|1649083212769; PCSYCityID=CN_0_0_0; areaId=19; ipLoc-djd=19-1609-0-0; __jdc=122270672; shshshfp=424f5ca92b154baf01526393cf7aabe0; rkv=1.0; __jda=122270672.16448122780871481326052.1644812278.1649492785.1649498931.10; 3AB9D23F7A4B3C9B=X2QPCC3XC2WH6X2K3NDHNTZJVJQ2UJKLELT4WLF46ZA7AY2QKYFY3L4RBYDCYG5PS6EG4UBMUIUCS5XUHSPN5RHTXY; __jdb=122270672.4.16448122780871481326052|10.1649498931; shshshsID=363e3c864b090fe29d0ea950ad688a08_4_1649500200031'
}

r = requests.get(url, headers=headervalue)
soup = BeautifulSoup(r.text, 'lxml')

# print(r.headers)
soup = BeautifulSoup(r.text, 'lxml')
node_li = soup.select('.gl-item')

for li in node_li:
    p_name = li.select('.p-name em')[0].text.strip()
    p_price = li.select('.p-price strong')[0].text.strip()
    p_shopnum = li.select('.p-shopnum a')[0].text
    print(p_name, p_price, p_shopnum)
