import requests
import re

url = "https://www.baidu.com/"
headervalue = {
    "User-Agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.46',
}

r = requests.get(url, headers=headervalue)
html = r.text

# print(html)

item = re.search('<div.*?s-top-left.*?a.*?href="(.*?)".*?>(.*?)</a>', html, re.S)

print(item.group(1), item.group(2))