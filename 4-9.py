import pymongo

myclient = pymongo.MongoClient(host='192.168.3.110', port=27017)
mydb = myclient.test
collection = mydb.student
list = [
    {'id': '001', 'name': '小明', 'age': 10},
    {'id': '002', 'name': '小红', 'age': 11},
    {'id': '003', 'name': '小刚', 'age': 11},
    {'id': '004', 'name': '小蓝', 'age': 12},
]

collection.insert_many(list)

for i in collection.find():
    print(i)

print('第一个age为11的记录', collection.find_one({'age': 11}))
print('所有age为11的记录：')

for i in collection.find({'age': 11}):
    print(i)

# 删除id 003的记录
collection.delete_one({'id': '003'})
for i in collection.find():
    print(i)

# 更新 age 12的记录修改成 age=10
collection.update_one({'age': 12}, {'$set': {'age': 10}})
print('讲第一个age12的记录修改成age为10后的所有记录：')
for i in collection.find():
    print(i)

print('输出排序后记录：')
for i in collection.find().sort('age'):
    print(i)