from bs4 import BeautifulSoup
import requests

# 文档:https://www.crummy.com/software/BeautifulSoup/bs4/doc/index.zh.html

url = 'https://beijing.qfang.com/newhouse/list'

r = requests.get(url)
soup = BeautifulSoup(r.text, 'lxml')

print('soup类型: ', type(soup))
print('soup li类型: ', type(soup.li))
print('li 节点: ', soup.li)
print('li 节点的name 属性: ', soup.li.name)
print('li 节点的contents 属性: ', soup.li.contents)
print('li 节点的string 属性: ', soup.li.string)
print('li 节点的attrs 属性: ', soup.li.attrs)
print('li 节点的a节点的string 属性: ', soup.li.a.string)