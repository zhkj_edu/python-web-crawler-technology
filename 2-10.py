import requests

paramValue = {
    'q': 'python',
}

headValue = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36 Edg/98.0.1108.55'
}

r = requests.get('https://www.douban.com/', params=paramValue)
print('请求的url：', r.url)
print('响应状态码：', r.status_code)
print('请求头：', r.request.headers)

r = requests.get('https://www.douban.com/',params=paramValue, headers=headValue)
print('请求的url：', r.url)
print('响应状态码：', r.status_code)
print('请求头：', r.request.headers)