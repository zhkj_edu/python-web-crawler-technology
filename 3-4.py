import requests
import chardet
from lxml import etree

chapter_url = 'https://www.bqkan8.com/50_50096/18520412.html'
r = requests.get(chapter_url)
code_type = r.apparent_encoding
print(code_type)
if code_type == 'GB2312' :
    r.encoding = 'GBK'

html = etree.HTML(r.text)
title = html.xpath('//h1/text()')
print(title)

contents = html.xpath('//div[@id="content"]/text()')
for i in contents:
    content = i.strip()
    content = content.replace('&1t;/p>','')
    print(content)