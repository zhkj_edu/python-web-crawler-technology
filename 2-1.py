import urllib.request

url = 'http://fanyi.youdao.com'

response = urllib.request.urlopen(url)
print('响应类型：', type(response))
print('响应状态码：', response.getcode())
print('编码方式：', response.getheader('Content-Type'))
print('请求URL：', response.geturl())

resp = response.read().decode('utf-8')
print('响应内容：', resp)
