from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import time

browser = webdriver.Edge()
wait = WebDriverWait(browser, 10)

browser.get('https://www.jd.com/')
time.sleep(2)

element = WebDriverWait(browser, 10).until(
    EC.presence_of_all_elements_located(
        (By.CSS_SELECTOR, '.cate_menu_item a')
    )
)[0]

element.click()
handles = browser.window_handles
browser.switch_to.window(handles[-1])
browser.implicitly_wait(10)

browser.execute_script("window.scrollTo(0,document.body.scrollHeight);")
time.sleep(2)

titles = browser.find_elements(By.CSS_SELECTOR, '.goods-item__title')
count_line = 0
for title in titles:
    if title.text != '':
        count_line += 1
        print(title.text)

print("共找到:", count_line, "条数据")

