import requests
from lxml import etree

# 三国演义
url = 'https://www.bqkan8.com/50_50096/'
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/98.0.4758.102 Safari/537.36 Edg/98.0.1108.62',
}

r = requests.get(url, timeout=10, headers=headers)
r.encoding = 'GBK'
html = etree.HTML(r.content)
print('HTML对象类型: ', type(html))

print('body节点下所有div子节点: ', html.xpath('/html/body/div'))
print('body节点下所有div子孙节点: ', html.xpath('/html//div'))
print('body节点下所有子节点: ', html.xpath('/html/*'))
