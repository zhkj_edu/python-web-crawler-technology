from bs4 import BeautifulSoup
import requests
#正则表达式模块
import re

# 文档:https://www.crummy.com/software/BeautifulSoup/bs4/doc/index.zh.html

url = 'https://beijing.qfang.com/newhouse/list'

r = requests.get(url)
soup = BeautifulSoup(r.text, 'lxml')

# 书上是找不到东西的哈，改成这个
print('div下所有house-title节点个数: ', len(soup.find_all('a', attrs={'class': 'house-title'})))

node_a = soup.select('.list-main-header')[0].select('a')
# print(node_a)
print('第一个a标签属性值：', node_a[0]['href'])
print('第一个a标签文本：', node_a[0].text.strip())