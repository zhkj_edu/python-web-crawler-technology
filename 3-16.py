from bs4 import BeautifulSoup, Tag
import requests
import csv


# 文档:https://www.crummy.com/software/BeautifulSoup/bs4/doc/index.zh.html

def get_house(house: Tag):
    try:
        house_list = [house.select_one('.house-title em').text]
        # print(house.select_one('.house-title em').text)

        house_metas = house.select('[class="house-metas clearfix"] p')
        for item in house_metas:
            # print(item.string.strip())
            house_list.append(item.string.strip())

        list_price = house.select('[class="list-price"] span')
        # print(list_price[0].string + list_price[1].string)
        house_list.append(list_price[0].string + list_price[1].string)

        info = house.select('[class="new-house-info"] a')

        for item in info:
            # print("i:", item.text.strip())
            info_str = item.text.strip()
            if info_str != '\ue624查看地图':
                house_list.append(info_str)

        save_csv_file(house_list)
        print('--------------------------------------')
    except:
        pass


def save_csv_file(list):
    print(list)
    with open('house_info.csv', 'a', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        writer.writerow(list)


url = 'https://beijing.qfang.com/newhouse/list'
# 页面从n1->n9
for i in range(1, 10):
    print('页面：', i)
    r = requests.get(url + '/n' + str(i))
    soup = BeautifulSoup(r.text, 'lxml')
    houses = soup.select('.list-result li')

    for house in houses:
        get_house(house)
