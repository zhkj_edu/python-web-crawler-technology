import requests
import re


url = "https://www.baidu.com/"
headervalue = {
    "User-Agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.46',
}

r = requests.get(url, headers=headervalue)
html = r.text

pattern = re.compile('<span.*?title-content-index.*?>(.*?)</span>.*?title-content-title.*?>(.*?)</span>')

items = re.findall(pattern, html)
for item in items:
    print(item)
