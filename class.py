# 声明一个类: 鸭子类型
class Duck:
    name = ''

    # 初始化函数

    def __init__(self, name):
        self.name = name
        print("一只小鸭子出生了! 它的名字是: ", self.name)

    # 走路
    def walk(self):
        print("像一只鸭子一样走路")

    # 叫一声
    def speak(self):
        print("我是一直小鸭子~~~~,我的名字叫:", self.name)


# 实例化,或者叫获取一个对象 ----------------------------------------------------------------
duck1 = Duck('小鸭子1')
duck1.walk()
duck1.speak()