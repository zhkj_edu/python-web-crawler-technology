import time

from selenium import webdriver
from selenium.webdriver.common.by import By


browser = webdriver.Edge()

browser.get('https://www.taobao.com/')

# 定位q节点->旧方法
# print(browser.find_element_by_id('q'))
print(browser.find_element(By.ID, 'q'))


print(browser.find_element(By.CLASS_NAME, 'btn-search'))
print(browser.find_element(By.XPATH, '//input[@id="q"]'))
print(browser.find_element(By.CSS_SELECTOR, '.btn-search'))
element = browser.find_element(By.CSS_SELECTOR, '.btn-search')
print('class节点的属性type属性为', element.get_attribute("type"))
print('class节点的文本为', element.text)
print('class节点名为', element.tag_name)
print('class节点名id为', element.id)

time.sleep(5)