import chardet
import requests

base_url = 'https://movie.douban.com/top250'
headers_value = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36 Edg/99.0.1150.36',
}

for i in range(0, 10):
    param_value = {'start': str(i * 25), 'filter': ''}
    try:
        r = requests.get(base_url, params=param_value, headers=headers_value, timeout=1)
    except requests.Timeout:
        print('请求第页', i, '数据超时')
    else:
        print(r.status_code)
        print(r.url)
        code_type = chardet.detect(r.content)['encoding']
        with open('movie.txt', 'a+', encoding='utf-8') as f:
            f.write(r.content.decode(code_type))
