import requests

# get方法
r = requests.get('https://www.douban.com/')
print('响应类型：', type(r))
print('请求的url：', r.url)
print('响应状态码：', r.status_code)
print('请求头：', r.request.headers)
